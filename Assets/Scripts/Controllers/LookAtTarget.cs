﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtTarget : MonoBehaviour
{
    [SerializeField] private Transform _targerTransform;

    void LateUpdate()
    {
        transform.LookAt(_targerTransform);
    }

    public void SetTarget(Transform target)
    {
        _targerTransform = target;
    }
}
