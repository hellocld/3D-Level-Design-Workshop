﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour
{
    [SerializeField] private float _gravity = 20;
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpForce;

    private CharacterController _controller;
    private Animator _animator;
    private Vector3 _moveVector;

    private float _verticalVelocity;

    void Start()
    {
        _controller = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        _moveVector *= _speed;
        _verticalVelocity -= _gravity*Time.deltaTime;

        if (_controller.isGrounded)
        {
            _verticalVelocity = 0;
        }

        _moveVector.y = _verticalVelocity;
        _controller.Move(_moveVector*Time.deltaTime);

        if (_animator)
        {
            _animator.SetFloat("Velocity", Mathf.Abs(Vector3.Scale(_moveVector, new Vector3(1, 0, 1)).magnitude));
        }
    }


    public void Move(Vector3 move)
    {
        _moveVector = move;
    }

    public void Jump()
    {
        if (_controller.isGrounded)
        {
            _verticalVelocity = _jumpForce;
        }
    }
}
