﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField] private float _gravity = 20;
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpForce;
    
    private bool _isGrounded;


    private CharacterController _controller;
    private Animator _animator;
    private Transform _camTransform;
    private SkinnedMeshRenderer _renderer;
    private Vector3 _moveVector, _rotVector;
    private HealthManager _healthManager;
    private float _verticalVelocity;

    public bool _takingInput = true;
    private bool _invincible;
    private bool _bounced = false;

    void Start()
    {
        _renderer = GetComponentInChildren<SkinnedMeshRenderer>();
        _controller = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
        _camTransform = Camera.main.transform;
        _healthManager = GetComponent<HealthManager>();
        _healthManager._damageAction.AddListener(TakeDamage);
        _healthManager._deathAction.AddListener(Dead);
        _rotVector = transform.forward;
    }

    void Update()
    {
        if (_takingInput)
        {
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");
            Vector3 camForward = Vector3.Scale(_camTransform.forward, new Vector3(1, 0, 1)).normalized;
            _moveVector = (v*camForward + h*_camTransform.right);
            _moveVector *= _speed;
        }
        
        if (Vector3.Scale(_moveVector, new Vector3(1, 0, 1)).magnitude > 0.5)
        {
            _rotVector = Vector3.Scale(_moveVector, new Vector3(1, 0, 1)).normalized;
        }

        if (_controller.isGrounded)
        {
            _verticalVelocity = 0;
            if (Input.GetButton("Jump"))
            {
                _verticalVelocity = _jumpForce;
            }
        }

        if (_bounced)
        {
            Debug.Log("Bouncing...");
            _verticalVelocity = _jumpForce;
            _bounced = false;
        }

        _verticalVelocity -= _gravity*Time.deltaTime;
        _moveVector.y = _verticalVelocity;

        _controller.Move(_moveVector*Time.deltaTime);
        transform.rotation = Quaternion.SlerpUnclamped(
            transform.rotation, 
            Quaternion.LookRotation(_rotVector), 
            Time.deltaTime * 10);

        _animator.SetFloat(
            "Velocity", 
            Mathf.Abs(Vector3.Scale(_moveVector, new Vector3(1, 0, 1)).magnitude));


        _animator.SetFloat("Vertical Velocity", _verticalVelocity);
        _animator.SetBool("Grounded", _controller.isGrounded);
    }

    public void TakeDamage(GameObject obj)
    {
        Debug.Log("OUCH");
        if (!_invincible)
        {
            StartCoroutine(TakeDamageCoroutine(obj));
        }
    }

    public void Bounce()
    {
        _bounced = true;
    }

    public void Dead(GameObject obj)
    {
        Debug.Log("You're dead!");
    }

    public IEnumerator TakeDamageCoroutine(GameObject obj)
    {
        Vector3 normal = Vector3.Scale(obj.transform.position - transform.position, new Vector3(1, 0, 1));
        _takingInput = false;
        _animator.SetBool("Ouch", true);
        _moveVector -= normal*3;
        Bounce();
        Debug.DrawLine(transform.position, transform.position + (normal * 3));
        StartCoroutine(StrobeCoroutine());
        yield return new WaitForSeconds(1);
        _takingInput = true;
        _animator.SetBool("Ouch", false);
    }

    public IEnumerator StrobeCoroutine()
    {
        _invincible = true;
        for (int i = 0; i < 30; i++)
        {
            yield return new WaitForSeconds(0.01f);
            _renderer.enabled = false;
            yield return new WaitForSeconds(0.01f);
            _renderer.enabled = true;
        }
        _invincible = false;

    }

}
