﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlerpToRotation : MonoBehaviour
{
    [SerializeField] private Transform _targetTransform;

    void Start()
    {
        //transform.rotation = _targetTransform.rotation;
    }

    public void SetTarget(Transform target)
    {
        _targetTransform = target;
    }

    void LateUpdate()
    {
        transform.rotation = Quaternion.SlerpUnclamped(
            transform.rotation,
            _targetTransform.rotation,
            Time.deltaTime);
    }
}
