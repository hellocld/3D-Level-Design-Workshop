﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackToPosition : MonoBehaviour
{
    [SerializeField] private Transform _targetPosition;
    [SerializeField] private float _speed;
    [SerializeField] private bool _snap;

    private void Start()
    {
        //transform.position = _targetPosition.position;
    }

    public void SetTarget(Transform target)
    {
        _targetPosition = target;
    }

    void LateUpdate()
    {
        if (_snap)
        {
            transform.position = _targetPosition.position;
        }
        else
        {
            transform.position =
                Vector3.LerpUnclamped(
                    transform.position,
                    _targetPosition.position,
                    Time.deltaTime * _speed);
        }

    }
}
