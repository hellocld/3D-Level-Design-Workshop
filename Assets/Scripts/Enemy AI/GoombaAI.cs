﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoombaAI : MonoBehaviour
{
    [SerializeField] private float _wanderTurnSpeed, _huntTurnSpeed, _wanderSpeed, _huntSpeed;
    [SerializeField] private float _visionRadius = 1;
    [SerializeField] private LayerMask _visibleLayers;

    private bool _hunting = false;
    private bool _alive = true;

    private NPCController _controller;
    private Quaternion _targetAngle, _lastAngle;
    private float _lastTurnTime;
    private HealthManager _health;
    void Start()
    {
        _controller = GetComponent<NPCController>();
        _health = GetComponentInChildren<HealthManager>();
        _health._deathAction.AddListener(Dead);
        ResetAngle();
    }

    void Update()
    {

        if (!CanSeePlayer())
        {
            transform.rotation =
                Quaternion.Slerp(
                    _lastAngle,
                    _targetAngle,
                    (Time.time - _lastTurnTime)/_wanderTurnSpeed);

            if ((Time.time - _lastTurnTime)/_wanderTurnSpeed > 1)
            {
                ResetAngle();
            }
            _controller.Move(transform.forward * _wanderSpeed);
        }
        else
        {
            transform.rotation =
                Quaternion.Slerp(transform.rotation, _targetAngle, _huntTurnSpeed * Time.deltaTime);
            _controller.Move(transform.forward * _huntSpeed);
        }
    }

    void Dead(GameObject obj)
    {
        Debug.Log("Dead goomba");

        GetComponent<Animator>().SetTrigger("Splat");
        
        if (obj.transform.root.GetComponent<PlayerController>() != null)
        {
            obj.transform.root.GetComponent<PlayerController>().Bounce();
        }
        
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }

    void ResetAngle()
    {
        _lastAngle = transform.rotation;
        _targetAngle = Quaternion.Euler(0, Random.Range(-90, 90.0f), 0);
        _lastTurnTime = Time.time;
    }

    bool CanSeePlayer()
    {
        Collider[] cols = Physics.OverlapSphere(transform.position, _visionRadius, _visibleLayers);
        for (int i = 0; i < cols.Length; i++)
        {
            if (cols[i].CompareTag("Player"))
            {
                Debug.Log("Player visible");

                _targetAngle = Quaternion.LookRotation(
                    Vector3.Scale(cols[i].transform.position, new Vector3(1, 0, 1))
                    - Vector3.Scale(transform.position, new Vector3(1, 0, 1)));
                
                return true;
            }
        }
        return false;
    }
}
