﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnContact : MonoBehaviour
{

    [SerializeField] private int _damageApplied;

    private void Start()
    {
        foreach(Collider c in transform.root.GetComponentsInChildren<Collider>())
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), c);
        }
    }

    void OnTriggerEnter(Collider col)
    {
        HealthManager h = col.gameObject.GetComponent<HealthManager>();
        if (h != null)
        {
            Debug.Log("Healing!", gameObject);
            h.TakeDamage(_damageApplied, transform.gameObject);
        }
    }

}
