﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealOnContact : MonoBehaviour
{

    [SerializeField] private int _healthUp;
    [SerializeField] private bool _hideOnContact = true;

    void OnTriggerEnter(Collider col)
    {
        HealthManager h = col.gameObject.GetComponent<HealthManager>();
        if (h != null)
        {
            Debug.Log("Contact!", gameObject);
            h.Heal(_healthUp);
        }

        if (_hideOnContact)
        {
            gameObject.SetActive(false);
        }

    }
}
