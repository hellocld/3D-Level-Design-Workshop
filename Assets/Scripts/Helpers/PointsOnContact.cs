﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsOnContact : MonoBehaviour
{
    [SerializeField] private int _points;
    [SerializeField] private bool _disableOnContact = true;
    private void OnTriggerEnter(Collider other)
    {
        GameManager.Instance.AddToScore(_points);
        if (_disableOnContact)
        {
            gameObject.SetActive(false);
        }
    }
}
