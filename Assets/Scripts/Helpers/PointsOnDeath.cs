﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsOnDeath : MonoBehaviour
{
    [SerializeField] private int _score = 100;

    void Start()
    {
        transform.root.GetComponentInChildren<HealthManager>()._deathAction.AddListener((obj)=> {GameManager.Instance.AddToScore(_score);});
    }

}
