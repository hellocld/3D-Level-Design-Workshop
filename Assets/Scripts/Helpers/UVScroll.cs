﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UVScroll : MonoBehaviour {

    [SerializeField] private Material _mat;
    [SerializeField] private float _scrollSpeed = 1;
    void Update()
    {
        _mat.SetTextureOffset("_MainTex", new Vector2(0, Time.time % _scrollSpeed));
    }
	
}
