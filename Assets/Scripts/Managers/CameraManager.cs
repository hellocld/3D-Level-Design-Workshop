﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : Singleton<CameraManager> {

    private void Start()
    {
        GetComponent<SlerpToRotation>().SetTarget(GameObject.Find("Robro").transform);
        GetComponent<TrackToPosition>().SetTarget(GameObject.Find("Robro").transform);
        GetComponentInChildren<LookAtTarget>().SetTarget(GameObject.Find("LookAtTarget").transform);
    }

    public void SetTarget(Transform target)
    {
        GetComponent<SlerpToRotation>().SetTarget(target);
        GetComponent<TrackToPosition>().SetTarget(target);
    }

}
