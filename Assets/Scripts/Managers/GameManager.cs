﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class GameManager : Singleton<GameManager>
{

    [SerializeField] private int _maxLives = 1;

    public int _playerLives { get; private set; }
    public int _playerScore { get; private set; }
    private PlayerSpawnManager _playerSpawnManager;
    private GameObject _player = null;
    private List<GameObject> _levelEnemies;
    private bool _waitForKeypress;

    void Awake()
    {
        if (Instance.gameObject != this.gameObject)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

        _playerLives = _maxLives;

        SceneManager.sceneLoaded += SetScene;

    }

    private void Update()
    {
        if(_waitForKeypress && Input.GetButtonDown("Jump"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    private void LevelComplete()
    {
        Debug.Log("Level complete!");
        Time.timeScale = 0.05f;
        UIManager.Instance.LevelComplete();
    }

    private void PlayerDied(GameObject obj)
    {
        _playerLives--;
        if (_playerLives <= 0)
        {
            GameOver();
        }
        else
        {
            UIManager.Instance.YouDied();
            _waitForKeypress = true;
            Time.timeScale = 0.05f;
        }
    }

    private void GameOver()
    {
        Debug.Log("Game Over");
        UIManager.Instance.GameOver();
    }

    private void SetScene(Scene arg0, LoadSceneMode loadSceneMode)
    {
        _playerScore = 0;

        FindObjectOfType<LevelExitManager>()._levelExitEvent.AddListener(LevelComplete);

        _playerSpawnManager = FindObjectOfType<PlayerSpawnManager>();
        _player = _playerSpawnManager.SpawnPlayer();
        //_player = GameObject.Find("Robro");
        _player.GetComponent<HealthManager>()._deathAction.AddListener(PlayerDied);
        _waitForKeypress = false;
        Time.timeScale = 1;
    }

    public void AddToScore(int points)
    {
        _playerScore += points;
    }
}
