﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DamageEvent: UnityEvent<GameObject> { }

public class HealthManager : MonoBehaviour
{

    [SerializeField] private int _maxHealth;
    public DamageEvent _damageAction, _deathAction;
    public int _currentHealth { get; private set; }

    void Awake()
    {
        _damageAction = new DamageEvent();
        _deathAction = new DamageEvent();
        Reset();
    }

    public void Reset()
    {
        _currentHealth = _maxHealth;
    }

    public void TakeDamage(int damage, GameObject damagingObject)
    {
        _currentHealth -= damage;
        if (_currentHealth > 0)
        {
            _damageAction.Invoke(damagingObject);
        }
        else
        {
            _deathAction.Invoke(damagingObject);
        }

    }

    public void Heal(int healing)
    {
        Debug.Log("Healing...");
        _currentHealth += healing;
        if (_currentHealth > _maxHealth)
        {
            _currentHealth = _maxHealth;
        }
    }

}
