﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelExitManager : MonoBehaviour
{

    public UnityEvent _levelExitEvent;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _levelExitEvent.Invoke();
        }
    }
}
