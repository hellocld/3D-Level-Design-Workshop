﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawnManager : MonoBehaviour
{

    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private GameObject _playerCamObjectPrefab;
    [SerializeField] private Transform _spawnTransform;

    private GameObject _player, _playerCamObject;

    void Awake()
    {
        
    }

    public GameObject SpawnPlayer()
    {

        if (FindObjectOfType<PlayerController>() == null)
        {
            _player = Instantiate(_playerPrefab);
        }
        else
        {
            _player = FindObjectOfType<PlayerController>().gameObject;
        }
        if (FindObjectOfType<CameraManager>() == null)
        {
            _playerCamObject = Instantiate(_playerCamObjectPrefab);
            _playerCamObject.GetComponent<TrackToPosition>().SetTarget(_player.transform);
            _playerCamObject.GetComponent<SlerpToRotation>().SetTarget(_player.transform);
        }
        else
        {
            _playerCamObject = FindObjectOfType<CameraManager>().gameObject;
        }

        _player.transform.position = _spawnTransform.position;
        _player.transform.rotation = transform.rotation;

        _playerCamObject.transform.position = _spawnTransform.position;
        _playerCamObject.transform.rotation = transform.rotation;

        return _player;

    }

}
