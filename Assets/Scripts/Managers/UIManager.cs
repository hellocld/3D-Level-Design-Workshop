﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager> {
    [SerializeField] private Text _livesText, _mainText, _scoreText;
    [SerializeField] private GameObject _textPanel;
    [SerializeField] private GameObject[] _healthIcons;

    private PlayerController _player;
    private HealthManager _playerHealth;
    void Start()
    {
        _player = FindObjectOfType<PlayerController>();
        _textPanel.SetActive(false);
        _playerHealth = _player.GetComponent<HealthManager>();
    }


    public void Update()
    {
        _livesText.text = string.Format("Lives: {0}", GameManager.Instance._playerLives);
        _scoreText.text = string.Format("Score: {0}", GameManager.Instance._playerScore);

        for (int i = 0; i < 3; i++)
        {
            if (_playerHealth._currentHealth > i)
            {
                _healthIcons[i].SetActive(true);
            }
            else
            {
                _healthIcons[i].SetActive(false);
            }
        }

    }

    public void GameOver()
    {
        _mainText.text = "GAME OVER";
        _textPanel.SetActive(true);
    }

    public void YouDied()
    {
        _mainText.text = "You Died!\nPress 'Jump' to Restart";
        _textPanel.SetActive(true);
    }

    public void LevelComplete()
    {
        _mainText.text = "LEVEL COMPLETE!";
        _textPanel.SetActive(true);
    }
}
